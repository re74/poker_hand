#include <iostream>
#include <set>
#include <random>
#include <ctime>


struct Card
{
    std::string suit;
    std::string value;    
    // visualization is a list of strings, one per row
    std::vector<std::string> visualization = std::vector<std::string>(12);
    Card()
    {}
    Card (std::string s, std::string v)
    {
        value=v;
        suit=s;
        if (v.length()==2)
            v.insert(1, 1,' ');
        else 
            v=' '+v+' ';        

        if (s=="hearts")
            visualization = {
                "   ***   ***   ",
                "  ***** *****  ", 
                " ****** ****** ", 
                " ************* ",
                " ************* ", 
                " ************* ", 
                "  *** " + v + " ***  " ,
                "   *********   " ,
                "    *******    " ,
                "     *****     " ,
                "      ***      " ,
                "       *       " };

        else if (s=="spades")
            visualization = {
                "      ***      ",
                "     *****     ", 
                "    *******    ", 
                "   *********   ",
                "  ***********  ", 
                " **** *** **** ", 
                " **** " + v + " **** " ,
                "  *** *** ***  " ,
                "      ***      " ,
                "      ***      " ,
                "     *****     " ,
                "    *******    " };

        else if (s=="clubs")
            visualization = {
                "      ***      ",
                "     *****     ", 
                "     *****     ",
                "      ***      ", 
                "  *** *** ***  ", 
                " ************* ",
                "  *** " + v+ " ***  " ,
                "      ***      " ,
                "      ***      " ,
                "      ***      " ,
                "     *****     " ,
                "    *******    " };

        else
            visualization = {
                "      ***      ",
                "     *****     ", 
                "    *******    ",
                "   *********   ", 
                "  ***********  ", 
                " ************* ",
                " **** " + v + " **** " ,
                "  ***********  ", 
                "   *********   ", 
                "    *******    ",
                "     *****     " ,
                "      ***      " };    
    };
};

/*
*@desc prints a hand to the console
*@param a_hand vector holding 5 `Cards`
*/
void print_hand(const std::vector<struct Card> &a_hand)
{
    for (int i=0; i < 12; i++)
    {
        for (const auto &a_card : a_hand)
        {            
            std::cout << a_card.visualization[i] <<"    "; 
        }
        std::cout << std::endl;
    }
} 

/*
*@desc anayzes which combination the hand has. 
*@param a_hand Vector holding 5 `Cards`
*@returns a string from set {"Royal flush", "Straight flush", "Straight", "Flush", "Four of a kind", 
    "Full house", "Three", "Two pairs", "Lousy pair", "Trash hand"}
*/
std::string analyze_hand(const std::vector<struct Card> &a_hand)
{
    std::set <std::string> suit_set{};
    std::set <std::string> value_set{};
    
    std::vector<std::set <std::string>> street_sets =
    {{"2","3","4","5","6"},{"3","4","5","6","7"},{"4","5","6","7","8"},
    {"5","6","7","8","9"},{"6","7","8", "9","10"},{"7","8","9","10","J"},
    {"8","9","10","J","Q"},{"9","10","J","Q","K"},{"10","J","Q","K","A"}};
    
    
    for (const auto &a_card : a_hand)
    {   
        suit_set.insert(a_card.suit);
        value_set.insert(a_card.value);
    }
    
    if (suit_set.size()==1)
        if (street_sets[8]==value_set)
            return "Royal flush";

    for (auto &set : street_sets) 
        if (set==value_set)
            if (suit_set.size()==1)
                return "Straight flush";
            else
                return "Straight";
    
    if (suit_set.size()==1)
        return "Flush";
    
    if (value_set.size()==2)
    {
        int counter=0;
        for (const auto &a_card : a_hand)
        {   
            if (a_card.value==*std::next(value_set.begin(),0))
                counter++;
        }
        if (counter == 1 || counter == 4)
            return "Four of a kind";
        else
            return "Full house";
    }
    if (value_set.size() == 3)
    {
        int counter[3] ={0,0,0};
        for (const auto &a_card : a_hand)
        {   
            if (a_card.value== *std::next(value_set.begin(),0))
                counter[0]++;
            else if (a_card.value== *std::next(value_set.begin(), 1))
                counter[1]++;
            else
                counter[2]++;
        }        
        if (counter[0]==3 || counter[1]==3 || counter[2]==3)
            return "Three";
        else
            return "Two pairs";
    }
    if (value_set.size() == 4)
        return "Lousy pair";
    return "Trash hand";
} 

/*
*@desc The program generates a random hand (5 cards) drawn from the double deck, 
*presents the hand and its combination. 
*/
int main()
{
    std::vector<std::string> suits;
    suits={"hearts","clubs", "diamonds", "spades"};
    std::vector<std::string> values;
    values={ "2", "3","4","5","6","7","8","9","10","J","Q","K","A"};
    std::vector<struct Card> double_deck;
    for (int i=0; i < 2; i++)
        for ( auto v : values)
        {
            for ( auto s : suits)
            {
                struct Card a_card (s,v);
                double_deck.push_back(a_card);
            }
        }
    srand(time(0));
    std::vector <struct Card> hand(5);
    for (int i=0; i < 5; i++)
    {
        int random_idx=rand() % 104;
        hand[i] = double_deck[random_idx-i];
        double_deck.erase(double_deck.begin()+random_idx);
    }
    std::cout <<std::endl;
    print_hand(hand);
    std::cout <<std::endl << analyze_hand(hand)<<std::endl<<std::endl;
}