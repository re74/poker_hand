
# Poker hand

The program generates a random hand (5 cards) drawn from the double deck (104 cards).
The output outputs the poker hand to the console and also desribes it in terms of poker combinations, such as: 
- Royal flush.
- A, K, Q, J, 10, all the same suit.
- Straight flush. Five cards in a sequence, all in the same suit.
- Four of a kind. All four cards of the same rank.
- Full house. Three of a kind with a pair.
- Flush. All the cards of the same suit.
- Straight. Five cards in a sequence.
- Three of a kind.
- Two pairs.

Additionally:
- Lousy pair.
- Trash hand. 

For more information about poker: https://en.wikipedia.org/wiki/List_of_poker_hands



## Usage

1. Clone the repository.
Inside the src folder run
```sh
g++ main.cpp -o main
./main
```
2. Observe the output.
\
 *Output example*
```sh

      ***             ***   ***          ***   ***             ***             ***   ***       
     *****           ***** *****        ***** *****           *****           ***** *****      
     *****          ****** ******      ****** ******          *****          ****** ******     
      ***           *************      *************           ***           *************     
  *** *** ***       *************      *************       *** *** ***       *************     
 *************      *************      *************      *************      *************     
  ***  7  ***        ***  6  ***        ***  6  ***        ***  7  ***        ***  2  ***      
      ***             *********          *********             ***             *********       
      ***              *******            *******              ***              *******        
      ***               *****              *****               ***               *****         
     *****               ***                ***               *****               ***          
    *******               *                  *               *******               *           

Two pairs

```


## Maintainers

@MariaNema
